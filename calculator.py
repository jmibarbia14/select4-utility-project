import math

def option():
    print("----------------------------------------------------------------- \n"
    + "Select an operation sign to perform an arithmetic operation. \n"
    + "+ for ADDITION \n"
    + "- for SUBTRACTION \n"
    + "* for MULTIPLICATION \n"
    + "/ for DIVISION")

def operate():
    operation = input("\nOperation: ")

    if operation == '+':
        return operation
    elif operation == '-':
        return operation
    elif operation == '*':
        return operation
    elif operation == '/':
        return operation
    else:
        print("Invalid input. Please try again.")
        operate()

def add(num1, num2):
    result = num1 + num2
    result = round(result, 2)
    print('{} + {} = {}'.format(num1, num2, result))

def subtract(num1, num2):
    result = num1 - num2
    result = round(result, 2)
    print('{} - {} = {}'.format(num1, num2, result))

def multiply(num1, num2):
    result = num1 * num2
    result = round(result, 2)
    print('{} * {} = {}'.format(num1, num2, result))

def divide(num1, num2):
    result = num1 / num2
    result = round(result, 2)
    print('{} / {} = {}'.format(num1, num2, result))

def repeat():
    print("-----------------------------------------------------------------")
    print("Do you want to calculate again?")
    resp = input("Type Y for YES or N for NO: ")

    if resp.upper() == 'Y':
        calculate()
    elif resp.upper() == 'N':
        print("-----------------------------------------------------------------")
        print("Thank you. Bye.")
        exit()
    else:
        print("Invalid input. Please try again.")
        repeat()

def calculate():

    try:
        option()

        operation = operate()

        num1 = int(input("\nFirst number: "))
        num2 = int(input("Second number: "))

        if operation == '+':
            add(num1, num2)

        elif operation == '-':
            subtract(num1, num2)

        elif operation == '*':
            multiply(num1, num2)

        elif operation == '/':
            if num2 != 0:
                divide(num1, num2)
            else:
                print("Invalid input. Please try again.")
                calculate()

        else:
            print("Invalid input. Please try again.")
            calculate()

        repeat()
    except KeyboardInterrupt:
        print("\n-----------------------------------------------------------------")
        print("You cancelled the program.")
        print("Thank you. Bye.")
        exit()

def main():
    print("CALCULATOR")

    calculate()

main()
